//
//  AirplaneOptionViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "AirplaneOptionViewController.h"
#import "AirplaneOptionView.h"

@interface AirplaneOptionViewController ()

@property (nonatomic,strong) AirplaneOptionView *airplaneOptionView;

@end

@implementation AirplaneOptionViewController

-(void)loadView{
    self.airplaneOptionView = [[AirplaneOptionView alloc] init];
    self.view = self.airplaneOptionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
