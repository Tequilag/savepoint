//
//  MainView.h
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainView : UIView

@property(nonatomic,strong) UIButton *buttonSearch;
@property(nonatomic,strong) UIButton *ticketOptions;
@property(nonatomic,strong) UIButton *airplaneOptions;

@property(nonatomic,strong) UILabel *labelTo;
@property(nonatomic,strong) UILabel *labelFrom;
@property(nonatomic,strong) UILabel *labelDate;

@property(nonatomic,strong) UITextField *textFieldTo;
@property(nonatomic,strong) UITextField *textFieldFrom;
@property(nonatomic,strong) UITextField *datePickerField;

@property(nonatomic,strong) UIDatePicker *datePicker;

@property (nonatomic, assign) CGFloat topOffset;

@end
