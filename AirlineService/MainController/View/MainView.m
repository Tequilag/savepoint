//
//  MainView.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "MainView.h"

@implementation MainView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _buttonSearch = [[UIButton alloc] init];
    [_buttonSearch setTitle:@"Поиск" forState:UIControlStateNormal];
    _buttonSearch.backgroundColor = [UIColor grayColor];
    [self addSubview:_buttonSearch];
    
    _airplaneOptions = [[UIButton alloc] init];
    [_airplaneOptions setTitle:@"Параметры самолета" forState:UIControlStateNormal];
    _airplaneOptions.backgroundColor = [UIColor grayColor];
    [self addSubview:_airplaneOptions];
    
    _ticketOptions = [[UIButton alloc] init];
    [_ticketOptions setTitle:@"Параметры билета" forState:UIControlStateNormal];
    _ticketOptions.backgroundColor = [UIColor grayColor];
    [self addSubview:_ticketOptions];
    
    _labelFrom = [[UILabel alloc] init];
    _labelFrom.text = @"Откуда";
    [self addSubview:_labelFrom];
    
    _labelTo = [[UILabel alloc] init];
    _labelTo.text = @"Куда";
    [self addSubview:_labelTo];
    
    _labelDate = [[UILabel alloc] init];
    _labelDate.text = @"Дата вылета";
    [self addSubview:_labelDate];
    
    _textFieldFrom = [[UITextField alloc] init];
    _textFieldFrom.placeholder = @"Выберите город";
    _textFieldFrom.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:_textFieldFrom];
    
    _textFieldTo = [[UITextField alloc] init];
    _textFieldTo.placeholder = @"Выберите город";
    _textFieldTo.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:_textFieldTo];
    
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    _datePickerField = [[UITextField alloc] init];
    _datePickerField.placeholder = @"Выберите дату";
    _datePickerField.borderStyle = UITextBorderStyleRoundedRect;
    _datePickerField.inputView = self.datePicker;
    _datePickerField.text = [dateFormatter stringFromDate:[NSDate date]];
    [self addSubview:_datePickerField];
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat offset = 10.0f;
    CGFloat heightInputField = 30.0f;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat searchButtonWidth = width*0.5f;
    CGFloat searchButtonHeight = height*0.85f;
    CGFloat optionButonWidth = width * 0.4f;
    CGFloat labelWidth = 100.0f;
    
    self.labelFrom.frame = CGRectMake(offset,
                                      offset + self.topOffset,
                                      labelWidth,
                                      heightInputField);
    
    self.textFieldFrom.frame = CGRectMake(offset*2 + self.labelFrom.bounds.size.width,
                                          self.labelFrom.frame.origin.y,
                                          width - offset*2 - self.labelFrom.frame.origin.x - self.labelFrom.bounds.size.width ,
                                          heightInputField);
    
    self.labelTo.frame = CGRectMake(self.labelFrom.frame.origin.x,
                                    self.labelFrom.frame.origin.y + self.labelFrom.bounds.size.height + offset,
                                    labelWidth,
                                    heightInputField);
    
    self.textFieldTo.frame = CGRectMake(self.textFieldFrom.frame.origin.x,
                                        self.labelTo.frame.origin.y,
                                        self.textFieldFrom.bounds.size.width,
                                        heightInputField);
    
    self.labelDate.frame = CGRectMake(self.labelFrom.frame.origin.x,
                                      self.labelTo.frame.origin.y + self.labelTo.bounds.size.height + offset,
                                      labelWidth,
                                      heightInputField);
    
    self.datePickerField.frame = CGRectMake(self.textFieldFrom.frame.origin.x,
                                            self.labelDate.frame.origin.y,
                                            width - offset*2 - self.labelFrom.frame.origin.x - self.labelFrom.bounds.size.width ,
                                            heightInputField);
    
    self.ticketOptions.frame = CGRectMake(offset,
                                          self.datePickerField.frame.origin.y + self.datePickerField.bounds.size.height + offset,
                                          optionButonWidth,
                                          30.0f);
    self.airplaneOptions.frame = CGRectMake(offset*2 +self.ticketOptions.frame.origin.x + self.ticketOptions.bounds.size.width,
                                            self.ticketOptions.frame.origin.y,
                                            optionButonWidth,
                                            30.0f);
    
    self.buttonSearch.frame = CGRectMake((width - searchButtonWidth)/2.0f,
                                         searchButtonHeight,
                                         searchButtonWidth,
                                         50.0f);
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
