//
//  MainViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "MainViewController.h"
#import "MainView.h"
#import "CountryViewController.h"
#import "TicketOptionViewController.h"
#import "AirplaneOptionViewController.h"
#import "ResultViewController.h"

@interface MainViewController () <UITextFieldDelegate, CountryViewControllerDelegate>

@property (nonatomic,strong) MainView *mainView;

@end

@implementation MainViewController

- (void)loadView {
    self.mainView = [[MainView alloc]init];
    self.view = self.mainView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Поиск авиабилетов";
    
    self.mainView.textFieldTo.delegate = self;
    self.mainView.textFieldFrom.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"star"] style:UIBarButtonItemStyleDone target:self action:nil];
    
    [self.mainView.datePicker addTarget:self action:@selector(dateValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.mainView.ticketOptions addTarget:self action:@selector(changeTicketOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.airplaneOptions addTarget:self action:@selector(changeAirplaneOption:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.buttonSearch addTarget:self action:@selector(getResult:) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CountryViewController *countryController = [[CountryViewController alloc] init];
    countryController.typeOfController = CountryViewControllerTypeCountry;
    countryController.delegate = self;
    [self.navigationController pushViewController:countryController animated:YES];
    [self.view endEditing:YES];
    return NO;
}

-(void)dateValueChanged:(UIDatePicker *) sender{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yyyy"];
    self.mainView.datePickerField.text = [dateFormat stringFromDate:sender.date];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

-(void)changeTicketOption:(UIButton *) sender{
    TicketOptionViewController *ticketOptionViewController = [[TicketOptionViewController alloc] init];
    [self.navigationController pushViewController:ticketOptionViewController animated:YES];
    [self.view endEditing:YES];
}

-(void)changeAirplaneOption:(UIButton *) sender{
    AirplaneOptionViewController *airplaneOptionViewController = [[AirplaneOptionViewController alloc] init];
    [self.navigationController pushViewController:airplaneOptionViewController animated:YES];
    [self.view endEditing:YES];
}

-(void)getResult:(UIButton *) sender{
    ResultViewController *resultViewController = [[ResultViewController alloc] init];
    [self.navigationController pushViewController:resultViewController animated:YES];
    [self.view endEditing:YES];
}

-(void)getCity:(CountryViewController *)controller{
    if (self.mainView.textFieldTo.isFirstResponder)
    {
        self.mainView.textFieldTo.text = @"qwer";
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.mainView.topOffset = self.topLayoutGuide.length;
}

@end
