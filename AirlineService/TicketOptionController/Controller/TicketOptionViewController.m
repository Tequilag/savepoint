//
//  TicketOptionViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "TicketOptionViewController.h"
#import "TicketOptionView.h"

@interface TicketOptionViewController ()

@property (nonatomic,strong) TicketOptionView *ticketOptionView;

@end

@implementation TicketOptionViewController

-(void)loadView{
    self.ticketOptionView = [[TicketOptionView alloc] init];
    self.view = self.ticketOptionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
