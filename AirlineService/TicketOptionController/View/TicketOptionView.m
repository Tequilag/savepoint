//
//  TicketOptionView.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "TicketOptionView.h"

@implementation TicketOptionView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    return self;
}

@end
