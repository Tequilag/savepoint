//
//  CountryViewController.h
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CountryViewControllerTypeCountry,
    CountryViewControllerTypeCity,
} CountryViewControllerType;

@protocol  CountryViewControllerDelegate;

@interface CountryViewController : UIViewController

@property (nonatomic, assign) CountryViewControllerType typeOfController;
@property (nonatomic, weak) id<CountryViewControllerDelegate> delegate;

@end

@protocol CountryViewControllerDelegate <NSObject>

- (void)getCity:(CountryViewController *)controller;

@end
