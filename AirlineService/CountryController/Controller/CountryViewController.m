//
//  CountryViewController.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryView.h"
#import "AppDelegate.h"
//#import "CoreData/CoreData.h"

@interface CountryViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) CountryView *countryView;
//@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, weak) NSArray *countries;
@property (nonatomic, weak) NSArray *cities;

@end

@implementation CountryViewController

-(void)loadView{
    self.countryView = [[CountryView alloc] init];
    self.view = self.countryView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if (self.typeOfController == CountryViewControllerTypeCountry){
        self.navigationItem.title = @"Выберите страну";
    }
    else self.navigationItem.title = @"Выберите город";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location"] style:UIBarButtonItemStyleDone target:self action:@selector(getLocation:)];
    
    self.countryView.countryTable.dataSource = self;
    self.countryView.countryTable.delegate = self;
    
    self.countryView.searchBarCountry.delegate = self;
    
//        Выборка всех стран и городов
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Countries" inManagedObjectContext:context];
    [request setEntity:entity];
    _countries = [context executeFetchRequest:request error:nil];
    entity=[NSEntityDescription entityForName:@"Cities" inManagedObjectContext:context];
    [request setEntity:entity];
    _cities = [context executeFetchRequest:request error:nil];
}

-(NSManagedObjectContext *) managedObjectContext{
    CoreDataController *controller = [CoreDataController sharedController];
    return controller.managedObjectContext;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
    if (self.typeOfController == CountryViewControllerTypeCountry){
        return _countries.count;
        //return 5;
    }
    else return 7;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    NSString *str = [self.countries objectAtIndex:indexPath.row];
    cell.textLabel.text = str;
    cell.textLabel.text = @"QQ";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.typeOfController == CountryViewControllerTypeCountry)
    {
        CountryViewController *cityViewController = [[CountryViewController alloc] init];
        cityViewController.typeOfController = CountryViewControllerTypeCity;
        cityViewController.delegate = self.navigationController.viewControllers[0];
        [self.navigationController pushViewController:cityViewController animated:YES];

    }
    else{
        if (self.delegate) {
            [self.delegate getCity:self];
            
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
    [searchBar setShowsCancelButton:NO animated:YES];
}

-(void)getLocation:(UIBarButtonItem *) sender{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Teeeeeest"
                                 message:@"test message"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.countryView.topOffset = self.topLayoutGuide.length;
}


@end
