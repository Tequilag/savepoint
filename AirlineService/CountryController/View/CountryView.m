//
//  CountryView.m
//  AirlineService
//
//  Created by Андрей on 09.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "CountryView.h"

@implementation CountryView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _countryTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self addSubview:_countryTable];
    
    _searchBarCountry = [[UISearchBar alloc] init];
    _searchBarCountry.placeholder = @"Введите название";
    [_searchBarCountry setValue:@"Отмена" forKey:@"_cancelButtonText"];
    [self addSubview:_searchBarCountry];

    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.searchBarCountry.frame = CGRectMake(0, self.topOffset, self.bounds.size.width, 40.0f);
    self.countryTable.frame = CGRectMake(0, self.searchBarCountry.bounds.size.height, self.bounds.size.width, self.bounds.size.height - self.searchBarCountry.bounds.size.height);
    
    
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
