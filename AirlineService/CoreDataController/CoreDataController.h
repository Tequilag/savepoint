//
//  CoreDataController.h
//  AirlineService
//
//  Created by Gorbenko Georgy on 10.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CoreDataController : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

+ (instancetype)sharedController;

- (void)initializeCoreData;

@end
