//
//  CoreDataController.m
//  AirlineService
//
//  Created by Gorbenko Georgy on 10.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "CoreDataController.h"

@implementation CoreDataController

+ (instancetype)sharedController {
    static CoreDataController *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[CoreDataController alloc] init];
    });
    return sharedController;
}

- (id)init {
    self = [super init];
    
    if(!self) {
        return nil;
    }
    
    [self initializeCoreData];
    [self initializeJSONData];
    return self;
}

- (void)initializeCoreData{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AirlineService" withExtension:@"momd"];
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(mom != nil, @"Error initializing Managed Object Model");
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [moc setPersistentStoreCoordinator:psc];
    [self setManagedObjectContext:moc];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [documentsURL URLByAppendingPathComponent:@"AirlineService.sqlite"];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        NSError *error = nil;
        NSPersistentStoreCoordinator *psc = [[self managedObjectContext] persistentStoreCoordinator];
        NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType
                                             configuration:nil URL:storeURL
                                                           options:nil
                                                             error:&error];
        NSAssert(store != nil, @"Error initializing PSC: %@\n%@", [error localizedDescription], [error userInfo]);
    });
}

-(void)initializeJSONData{
    
}

@end
