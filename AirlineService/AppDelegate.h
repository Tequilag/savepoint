//
//  AppDelegate.h
//  AirlineSearchService
//
//  Created by Gorbenko Georgy on 06.12.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (nonatomic, strong) CoreDataController *coreDataController;

//@property (readonly, strong) NSPersistentContainer *persistentContainer;

//- (void)saveContext;


@end

